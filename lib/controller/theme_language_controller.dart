import 'package:aibooks/commons/theme_app/app_theme.dart';
import 'package:aibooks/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ThemeAndLanguageController extends GetxController {
  Locale localeApp = Locale('vi');
  ThemeData themeData = AppTheme.lightTheme;

  void setLocale(String language) {
    if (!S.delegate.supportedLocales.contains(Locale.fromSubtags(languageCode: language))) return;
    localeApp = Locale(language);
    Get.updateLocale(Locale(language));
    update();
  }

  void changeTheme(bool isLightTheme) {
    if (isLightTheme) {
      themeData = AppTheme.lightTheme;
      Get.changeTheme(AppTheme.lightTheme);
    } else {
      themeData = AppTheme.darkTheme;
      Get.changeTheme(AppTheme.darkTheme);
    }
    update();
  }
}
