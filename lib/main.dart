import 'package:aibooks/controller/theme_language_controller.dart';
import 'package:aibooks/generated/l10n.dart';
import 'package:aibooks/ui/screens/account/signin/signin_screen.dart';
import 'package:aibooks/ui/screens/account/signin/signin_viewmodel.dart';
import 'package:aibooks/ui/screens/account/signup/signup_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:get_it/get_it.dart';
import 'commons/check_internet/no_internet_controller.dart';
import 'commons/network_config/show_dialog_controller.dart';
import 'commons/screen_config/screen.dart';
import 'commons/screen_config/screen_utils.dart';
import 'ui/screens/account/changepassword/change_pass_viewmodel.dart';
import 'ui/screens/account/signin/login_viewmodel.dart';

final getIt = GetIt.instance;

void main() {
  setupGetIt();
  setGet();
  runApp(MyApp());
}

void setupGetIt() {
  // register all viewmodel here
  // generic instances were needed for viewmodel dependencies injection
  // Get.put(getIt<VM>())
  getIt
    ..registerFactory(() => ChangePassVM())
    ..registerFactory(() => SignInViewModel())
    ..registerFactory(() => LoginViewModel())
    ..registerFactory(() => SignUpViewModel());

  // register all common controller here
  // controller instances were needed for controller dependencies injection
  // Get.lazyPut(() => getIt<LastCountController>());
  getIt.registerLazySingleton(() => NoInternetController());
  getIt.registerLazySingleton(() => ThemeAndLanguageController());
  getIt.registerLazySingleton(() => ShowDialogController());
}

void setGet() {
  Get.lazyPut(() => getIt<NoInternetController>(), fenix: true);
  Get.lazyPut(() => getIt<ThemeAndLanguageController>(), fenix: true);
  Get.lazyPut(() => getIt<ShowDialogController>(), fenix: true);
}

class MyApp extends StatefulWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ThemeAndLanguageController>(
      builder: (ThemeAndLanguageController controller) {
        return GetMaterialApp(
          title: 'AIBook',
          defaultTransition: Transition.native,
          localizationsDelegates: const [
            S.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          locale: controller.localeApp,
          supportedLocales: S.delegate.supportedLocales,
          builder: (BuildContext context, Widget child) {
            ScreenUtils.init(context,
                width: Screen.checkDesignSize(context).width,
                height: Screen.checkDesignSize(context).height,
                allowFontScaling: true);
            final data = MediaQuery.of(context).copyWith(textScaleFactor: 1.0);
            return MediaQuery(data: data, child: child);
          },
          home: MyHomePage(),
        );
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            ScreenUtils.init(context,
                width: Screen.checkDesignSize(context).width,
                height: Screen.checkDesignSize(context).height,
                allowFontScaling: true);
            return SignInPage();
          },
        );
      },
    );
  }
}
