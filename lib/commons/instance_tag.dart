import 'package:get/get.dart';

String createInstanceTag<T>(String createdTime) {
  final bool isRegistered = Get.isRegistered<T>(tag: createdTime);
  if (!isRegistered) {
    return createdTime;
  }
  return createInstanceTag<T>('${createdTime}1');
}