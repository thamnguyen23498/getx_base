import 'package:aibooks/commons/check_horizontal.dart';
import 'package:aibooks/commons/check_internet/network_widget.dart';
import 'package:aibooks/commons/screen_config/screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_device_type/flutter_device_type.dart';
import 'package:get/get.dart';
import '../../main.dart';
import '../instance_tag.dart';
import 'base_viewmodel.dart';

// ignore: must_be_immutable
abstract class BasePageStateful<VM extends BaseViewModel> extends StatefulWidget {
  String instanceTag;
  VM viewModel;
  bool isTablet;
  bool isTabLetLandScape;

  @override
  _BasePageState createState() => _BasePageState<VM>();

  @protected
  Widget buildBody(BuildContext context);

  @protected
  void initPage() {}

  @protected
  void disposePage() {}
}

class _BasePageState<VM extends BaseViewModel> extends State<BasePageStateful<VM>> {
  String instanceTag = createInstanceTag(DateTime.now().toString());

  @override
  void initState() {
    widget
      ..instanceTag = instanceTag
      ..viewModel = Get.put(getIt<VM>(), tag: instanceTag)
      ..isTablet = Device.get().isTablet
      ..isTabLetLandScape = CheckHorizontal.isLandscapeTablet(context);
    super.initState();
    widget.initPage();
  }

  @override
  void didUpdateWidget(BasePageStateful<VM> oldWidget) {
    super.didUpdateWidget(oldWidget);
    widget
      ..instanceTag = instanceTag
      ..viewModel = Get.find(tag: instanceTag)
      ..isTablet = Device.get().isTablet
      ..isTabLetLandScape = CheckHorizontal.isLandscapeTablet(context);
  }

  @override
  Widget build(BuildContext context) {
    return NetworkWidget(child: widget.buildBody(context));
  }

  @override
  void dispose() {
    Get.delete<VM>();
    widget.disposePage();
    super.dispose();
  }
}
