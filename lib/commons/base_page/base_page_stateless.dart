import 'package:aibooks/commons/check_internet/network_widget.dart';
import 'package:aibooks/commons/screen_config/screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_device_type/flutter_device_type.dart';
import 'package:get/instance_manager.dart';
import '../../main.dart';
import '../check_horizontal.dart';
import '../instance_tag.dart';
import 'base_viewmodel.dart';

// ignore: must_be_immutable
abstract class BasePageStateless<VM extends BaseViewModel> extends StatelessWidget {
  BasePageStateless() {
    instanceTag = createInstanceTag(DateTime.now().toString());
    viewModel = Get.put(getIt<VM>(), tag: instanceTag);
  }

  String instanceTag;
  VM viewModel;
  bool isTablet = Device.get().isTablet;
  bool isTabLetLandScape;

  @override
  Widget build(BuildContext context) {
    isTabLetLandScape = CheckHorizontal.isLandscapeTablet(context);
    return NetworkWidget(
      child: buildBody(context),
    );
  }

  @protected
  Widget buildBody(BuildContext context);
}
