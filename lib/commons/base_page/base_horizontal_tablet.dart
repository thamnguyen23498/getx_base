import 'package:aibooks/commons/check_horizontal.dart';
import 'package:aibooks/commons/check_internet/network_widget.dart';
import 'package:aibooks/commons/screen_config/screen.dart';
import 'package:aibooks/commons/screen_config/screen_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_device_type/flutter_device_type.dart';
import 'package:get/get.dart';
import '../../main.dart';
import '../instance_tag.dart';
import 'base_viewmodel.dart';

// ignore: must_be_immutable
abstract class BaseTabletStateful<VM extends BaseViewModel> extends StatefulWidget {
  String instanceTag;
  VM viewModel;

  @override
  _BaseTabletState createState() => _BaseTabletState<VM>();

  @protected
  Widget buildBodyPhone(BuildContext pageContext);

  @protected
  Widget buildBodyTablet(BuildContext pageContext);

  @protected
  void initPage() {}

  @protected
  void disposePage() {}
}

class _BaseTabletState<VM extends BaseViewModel> extends State<BaseTabletStateful<VM>> {
  String instanceTag = createInstanceTag(DateTime.now().toString());
  bool isHorizontalTablet;

  @override
  void initState() {
    widget
      ..instanceTag = instanceTag
      ..viewModel = Get.put(getIt<VM>(), tag: instanceTag);
    super.initState();
    widget.initPage();
  }

  @override
  void didUpdateWidget(BaseTabletStateful<VM> oldWidget) {
    super.didUpdateWidget(oldWidget);
    widget
      ..instanceTag = instanceTag
      ..viewModel = Get.find(tag: instanceTag);
  }

  @override
  Widget build(BuildContext context) {
    isHorizontalTablet = CheckHorizontal.isLandscapeTablet(context);
    return NetworkWidget(
      child: isHorizontalTablet ? widget.buildBodyTablet(context) : widget.buildBodyPhone(context),
    );
  }

  // LayoutBuilder buildLayoutBuilder() {
  //   return LayoutBuilder(
  //     builder: (BuildContext context, BoxConstraints constraints) {
  //       return OrientationBuilder(
  //         builder: (context, orientation) {
  //           ScreenUtil.init(
  //             BoxConstraints(
  //               maxHeight: Screen.height(context),
  //               maxWidth: Screen.width(context),
  //             ),
  //             designSize: Screen.checkDesignSize(),
  //           );
  //           return NetworkWidget(
  //             child: isHorizontalTablet
  //                 ? widget.buildBodyPhone(context)
  //                 : widget.buildBodyPhone(context),
  //           );
  //         },
  //       );
  //     },
  //   );
  // }

  @override
  void dispose() {
    Get.delete<VM>();
    widget.disposePage();
    super.dispose();
  }

  void scale() {
    print('height: ${setHeight(1)}');
    print('width: ${setWidth(1)}');
    print('fontSize: ${setSp(1)}');
    print('device width: ${Device.screenWidth}');
    print('device height: ${Device.screenHeight}');
  }
}
