import 'package:aibooks/commons/screen_config/screen_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_device_type/flutter_device_type.dart';

import '../check_horizontal.dart';

class Screen {
  static MediaQueryData mediaQuery(BuildContext context) {
    return MediaQuery.of(context);
  }

  static double width(BuildContext context) {
    return mediaQuery(context).size.width;
  }

  static double height(BuildContext context) {
    return mediaQuery(context).size.height;
  }

  static double scale(BuildContext context) {
    return mediaQuery(context).devicePixelRatio;
  }

  static double topSafeHeight(BuildContext context) {
    return mediaQuery(context).padding.top;
  }

  static double bottomSafeHeight(BuildContext context) {
    return mediaQuery(context).padding.bottom;
  }

  static double deviceHeight(BuildContext context) {
    return mediaQuery(context).padding.bottom +
        mediaQuery(context).size.height +
        mediaQuery(context).padding.top;
  }

  static updateStatusBarStyle(SystemUiOverlayStyle style) {
    SystemChrome.setSystemUIOverlayStyle(style);
  }

  static double get appBarHeight {
    AppBar appBar = AppBar(
      title: Text('Demo'),
    );
    return appBar.preferredSize.height;
  }

  static Size checkDesignSize(BuildContext context) {
    if (Device.get().isTablet) {
      if (CheckHorizontal.isLandscapeTablet(context)) {
        return Size(1024, 768);
      } else {
        return Size(768, 1024);
      }
    } else {
      if (CheckHorizontal.isLandscapeScreen(context)) {
        return Size(896, 414);
      } else {
        return Size(414, 896);
      }
    }
  }
}

double setSp(num fontSize) {
  return ScreenUtils().setSp(fontSize);
}

double setWidth(num width) {
  return ScreenUtils().setWidth(width);
}

double setHeight(num height) {
  return ScreenUtils().setHeight(height);
}

double setRadius(double radius) {
  return radius;
}

// double setSp(num fontSize) {
//   return ScreenUtil().setSp(fontSize);
// }
//
// double setWidth(num width) {
//   return ScreenUtil().setWidth(width);
// }
//
// double setHeight(num height) {
//   return ScreenUtil().setHeight(height);
// }
//
// double setRadius(double radius) {
//   return ScreenUtil().setSp(radius);
// }
