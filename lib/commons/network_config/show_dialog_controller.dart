import 'package:get/get.dart';

class ShowDialogController extends GetxController {
  bool showDialogInternet = false;
  bool isShowLoading = false;

  void changeDialogInternet() {
    showDialogInternet = false;
    update();
  }

  void showLoading(bool isShow) {
    isShowLoading = isShow;
  }
}
