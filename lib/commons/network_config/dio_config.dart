import 'dart:io';
import 'package:aibooks/commons/base_page/get_x_util.dart';
import 'package:aibooks/commons/network_config/widgets/no_internet_dialog.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../check_internet/check_connect.dart';
import 'show_dialog_controller.dart';

ShowDialogController showDialogController = GetXUtil.instance();
Dio http = Dio(
  BaseOptions(
      connectTimeout: 25000,
      receiveTimeout: 25000,
      baseUrl: 'http://192.168.2.100:9000',
      // Add base url
      responseType: ResponseType.json,
      contentType: ContentType.json.toString()),
)
  ..interceptors.add(LogInterceptor(
    request: false,
    requestHeader: false,
    responseHeader: false,
  ))
  ..interceptors.add(InterceptorsWrapper(onRequest: (request, handle) {
    CheckConnect.check().then((value) {
      print('value connect: ${value.toString()}');
      if (value) {
        return handle.next(request);
      } else {
        Get.dialog(DialogNoInternet());
      }
    });
  }, onResponse: (response, handle) {
    showDialogController.showLoading(true);
    Get.dialog(const Center(child: CircularProgressIndicator()), barrierDismissible: false);
    if (response.data != null) {
      return handle.next(response);
    }
  }, onError: (error, handle) {
    if (error is Exception) {
      try {
        if (error is DioError) {
          print(error.type);
          switch (error.type) {
            case DioErrorType.connectTimeout:
              // TODO: Handle this case.
              break;
            case DioErrorType.sendTimeout:
              // TODO: Handle this case.
              break;
            case DioErrorType.receiveTimeout:
              // TODO: Handle this case.
              break;
            case DioErrorType.response:
              print('response error');
              print(error.response);
              switch (error.response.statusCode) {
                case 401:
                  // add
                  break;
                case 500:
                  break;
                default:
                  break;
              }
              break;
              // TODO: Handle this case.
              break;
            case DioErrorType.cancel:
              // TODO: Handle this case.
              break;
            case DioErrorType.other:
              // TODO: Handle this case.
              break;
          }
        } else if (error is SocketException) {
        } else {}
      } on FormatException catch (e) {
        print(e);
      } catch (e) {
        print(e);
      }
    }
    return handle.next(error);
  }));
