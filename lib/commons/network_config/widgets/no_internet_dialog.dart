import 'package:aibooks/commons/screen_config/screen.dart';
import 'package:aibooks/style/color_utils.dart';
import 'package:aibooks/style/font_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DialogNoInternet extends StatelessWidget {
  const DialogNoInternet({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(setWidth(20)),
        ),
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        child: Container(
          decoration: new BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(setWidth(20)),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: setHeight(16),
                ),
                Text(
                  'Mất kết nối',
                  style: FontUtils.textStyleSizeBold_20,
                ),
                SizedBox(
                  height: setHeight(20),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 16, right: 16),
                  child: Text(
                    'Không có kết nối vui lòng kiểm tra lại',
                    textAlign: TextAlign.center,
                    style: FontUtils.textStyleSize_16,
                  ),
                ),
                SizedBox(
                  height: setHeight(25),
                ),
                Container(height: 0.5, color: ColorUtils.colorBlack.withOpacity(0.5)),
                GestureDetector(
                  onTap: () {
                    Get.back();
                  },
                  child: Container(
                    width: double.infinity,
                    alignment: Alignment.center,
                    color: Colors.transparent,
                    padding: EdgeInsets.only(top: setWidth(8), bottom: setWidth(16)),
                    child: Text(
                      'OK',
                      style: FontUtils.textStyleSizeBold_18.copyWith(color: ColorUtils.colorGreen),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
