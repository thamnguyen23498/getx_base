import 'package:aibooks/commons/screen_config/screen.dart';
import 'package:aibooks/style/color_utils.dart';
import 'package:flutter/material.dart';

class AppTheme {
  AppTheme._();

  static final ThemeData lightTheme = ThemeData(
    scaffoldBackgroundColor: ColorUtils.colorWhite,
    brightness: Brightness.light,
    textTheme: lightTextTheme,
    primaryColor: ColorUtils.colorGreen,
    accentColor: ColorUtils.colorGreen,
    unselectedWidgetColor: ColorUtils.colorGreyText,
    buttonColor: ColorUtils.colorGreen,
  );

  static final ThemeData darkTheme = ThemeData(
    scaffoldBackgroundColor: ColorUtils.colorBlack,
    brightness: Brightness.dark,
    textTheme: darkTextTheme,
  );

  static final TextTheme lightTextTheme = TextTheme(
      headline1: styleHead1Light,
      headline2: styleHead2Light,
      headline3: styleHead3Light,
      headline4: styleHead4Light,
      headline5: styleHead5Light,
      headline6: styleHead6Light,
      bodyText1: styleBody1Light,
      bodyText2: styleBody2Light,
      overline: styleOverLineLight,
      button: styleButtonLight,
      subtitle1: styleSubTitle1Light,
      subtitle2: styleSubTitle2Light,
      caption: styleCaptionLight);
  static final TextTheme darkTextTheme = TextTheme(
      headline1: styleHead1Dark,
      headline2: styleHead2Dark,
      headline3: styleHead3Dark,
      headline4: styleHead4Dark,
      headline5: styleHead5Dark,
      headline6: styleHead6Dark,
      bodyText1: styleBody1Dark,
      bodyText2: styleBody2Dark,
      overline: styleOverLineDark,
      button: styleButtonDark,
      subtitle1: styleSubTitle1Dark,
      subtitle2: styleSubTitle2Dark,
      caption: styleCaptionDark);

  static TextStyle styleHead1Light = TextStyle(fontSize: 40, color: ColorUtils.colorBlack);
  static TextStyle styleHead2Light = TextStyle(fontSize: 36, color: ColorUtils.colorBlack);
  static TextStyle styleHead3Light = TextStyle(fontSize: 34, color: ColorUtils.colorBlack);
  static TextStyle styleHead4Light = TextStyle(fontSize: 30, color: ColorUtils.colorBlack);
  static TextStyle styleHead5Light = TextStyle(fontSize: 24, color: ColorUtils.colorBlack);
  static TextStyle styleHead6Light = TextStyle(fontSize: 20, color: ColorUtils.colorBlack);
  static TextStyle styleSubTitle1Light = TextStyle(fontSize: 18, color: ColorUtils.colorBlack);
  static TextStyle styleSubTitle2Light = TextStyle(fontSize: 16, color: ColorUtils.colorBlack);
  static TextStyle styleBody1Light = TextStyle(fontSize: 16, color: ColorUtils.colorBlack);
  static TextStyle styleBody2Light = TextStyle(fontSize: 14, color: ColorUtils.colorBlack);
  static TextStyle styleButtonLight = TextStyle(fontSize: 16, color: ColorUtils.colorBlack);
  static TextStyle styleCaptionLight = TextStyle(fontSize: 14, color: ColorUtils.colorBlack);
  static TextStyle styleOverLineLight = TextStyle(fontSize: 12, color: ColorUtils.colorBlack);

  static TextStyle styleHead1Dark = TextStyle(fontSize: 40, color: ColorUtils.colorWhite);
  static TextStyle styleHead2Dark = TextStyle(fontSize: 36, color: ColorUtils.colorWhite);
  static TextStyle styleHead3Dark = TextStyle(fontSize: 34, color: ColorUtils.colorWhite);
  static TextStyle styleHead4Dark = TextStyle(fontSize: 30, color: ColorUtils.colorWhite);
  static TextStyle styleHead5Dark = TextStyle(fontSize: 24, color: ColorUtils.colorWhite);
  static TextStyle styleHead6Dark = TextStyle(fontSize: 20, color: ColorUtils.colorWhite);
  static TextStyle styleSubTitle1Dark = TextStyle(fontSize: 18, color: ColorUtils.colorWhite);
  static TextStyle styleSubTitle2Dark = TextStyle(fontSize: 16, color: ColorUtils.colorWhite);
  static TextStyle styleBody1Dark = TextStyle(fontSize: 16, color: ColorUtils.colorWhite);
  static TextStyle styleBody2Dark = TextStyle(fontSize: 14, color: ColorUtils.colorWhite);
  static TextStyle styleButtonDark = TextStyle(fontSize: 16, color: ColorUtils.colorWhite);
  static TextStyle styleCaptionDark = TextStyle(fontSize: 14, color: ColorUtils.colorWhite);
  static TextStyle styleOverLineDark = TextStyle(fontSize: 12, color: ColorUtils.colorWhite);
}
