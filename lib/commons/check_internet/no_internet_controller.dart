import 'package:get/get.dart';

class NoInternetController extends GetxController {
  RxBool showNoInternet = true.obs;
  RxBool firstTime = true.obs;

  void setShowNoInternet(bool value) {
    showNoInternet.value = value;
    update();
  }

  void setFirstTime(bool value) {
    firstTime.value = value;
    update();
  }
}
