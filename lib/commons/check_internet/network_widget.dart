import 'package:aibooks/commons/base_page/get_x_util.dart';
import 'package:aibooks/commons/screen_config/screen.dart';
import 'package:aibooks/style/color_utils.dart';
import 'package:aibooks/style/font_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:get/get.dart';

import 'no_internet_controller.dart';

// ignore: must_be_immutable
class NetworkWidget extends StatefulWidget {
  NetworkWidget({@required this.child});

  Widget child;

  @override
  _NetworkWidgetState createState() => _NetworkWidgetState();
}

class _NetworkWidgetState extends State<NetworkWidget> {
  NoInternetController noInternetController = GetXUtil.instance();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    Get.delete<NoInternetController>();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<NoInternetController>(
      builder: (_) {
        return OfflineBuilder(
          connectivityBuilder: (BuildContext context, ConnectivityResult value, Widget child) {
            bool visibleWidget = false;
            final bool connected = value != ConnectivityResult.none;
            if (noInternetController.showNoInternet.value) {
              if (connected == false) {
                visibleWidget = true;
              }
              if (connected == true) {
                visibleWidget = false;
              }
            }
            return Stack(
              children: [
                child,
                Visibility(
                  visible: visibleWidget,
                  child: Positioned(
                    left: 0,
                    right: 0,
                    child: SafeArea(
                      child: Dialog(
                        elevation: 4,
                        insetPadding: EdgeInsets.zero,
                        child: AnimatedContainer(
                          duration: Duration(milliseconds: 300),
                          height: 60,
                          color: connected ? Colors.green : ColorUtils.colorRed,
                          child: connected
                              ? Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      "Đã kết nối",
                                      style: FontUtils.textStyleSize_16.copyWith(
                                        color: ColorUtils.colorWhite,
                                      ),
                                    ),
                                  ],
                                )
                              : Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Spacer(),
                                    Text(
                                      "Mất kết nối. Vui lòng kiểm tra lại kết nối",
                                      style: FontUtils.textStyleSize_16.copyWith(
                                        color: ColorUtils.colorWhite,
                                      ),
                                    ),
                                    Spacer(),
                                    IconButton(
                                        icon: Icon(
                                          Icons.cancel_outlined,
                                          size: setWidth(24),
                                          color: Colors.white,
                                        ),
                                        onPressed: () {
                                          noInternetController.setShowNoInternet(false);
                                          setState(() {
                                            visibleWidget = false;
                                          });
                                        })
                                  ],
                                ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            );
          },
          child: widget.child,
        );
      },
    );
  }
}
