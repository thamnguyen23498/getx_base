import 'package:aibooks/commons/screen_config/screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_device_type/flutter_device_type.dart';

class CheckHorizontal {
  static bool isLandscapeTablet(BuildContext context) {
    if (Device.get().isTablet && Screen.width(context) >= 1000) {
      return true;
    } else {
      return false;
    }
  }

  static bool isLandscapeScreen(BuildContext context) {
    if (Device.get().isPhone && Screen.width(context) >= 500) {
      return true;
    } else {
      return false;
    }
  }
}
