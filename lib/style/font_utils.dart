import 'package:aibooks/commons/screen_config/screen.dart';
import 'package:aibooks/style/color_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FontUtils {
  static TextStyle textStyleSize_12 = TextStyle(
    fontSize: setSp(12),
    fontFamily: 'Roboto',
    color: ColorUtils.colorBlack,
  );
  static TextStyle textStyleSize_14 = TextStyle(
    fontSize: setSp(14),
    fontFamily: 'Roboto',
    color: ColorUtils.colorBlack,
  );
  static TextStyle textStyleSize_16 = TextStyle(
    fontSize: setSp(16),
    fontFamily: 'Roboto',
    color: ColorUtils.colorBlack,
  );
  static TextStyle textStyleSize_18 = TextStyle(
    fontSize: setSp(18),
    fontFamily: 'Roboto',
  );
  static TextStyle textStyleSize_20 = TextStyle(
    fontSize: setSp(20),
    fontFamily: 'Roboto',
    color: ColorUtils.colorBlack,
  );
  static TextStyle textStyleSize_22 = TextStyle(
    fontSize: setSp(22),
    fontFamily: 'Roboto',
    color: ColorUtils.colorBlack,
  );
  static TextStyle textStyleSize_24 = TextStyle(
    fontSize: setSp(24),
    fontFamily: 'Roboto',
    color: ColorUtils.colorBlack,
  );
  static TextStyle textStyleSize_26 = TextStyle(
    fontSize: setSp(26),
    fontFamily: 'Roboto',
    color: ColorUtils.colorBlack,
  );
  static TextStyle textStyleSize_28 = TextStyle(
    fontSize: setSp(28),
    fontFamily: 'Roboto',
    color: ColorUtils.colorBlack,
  );
  static TextStyle textStyleSizeBold_12 = TextStyle(
      fontSize: setSp(12),
      fontFamily: 'Roboto',
      fontWeight: FontWeight.bold,
      color: ColorUtils.colorBlack);
  static TextStyle textStyleSizeBold_14 = TextStyle(
      fontSize: setSp(14),
      fontFamily: 'Roboto',
      fontWeight: FontWeight.bold,
      color: ColorUtils.colorBlack);
  static TextStyle textStyleSizeBold_16 = TextStyle(
      fontSize: setSp(16),
      fontFamily: 'Roboto',
      fontWeight: FontWeight.bold,
      color: ColorUtils.colorBlack);
  static TextStyle textStyleSizeBold_18 = TextStyle(
      fontSize: setSp(18),
      fontFamily: 'Roboto',
      fontWeight: FontWeight.bold,
      color: ColorUtils.colorBlack);
  static TextStyle textStyleSizeBold_20 = TextStyle(
      fontSize: setSp(20),
      fontFamily: 'Roboto',
      fontWeight: FontWeight.bold,
      color: ColorUtils.colorBlack);
  static TextStyle textStyleSizeBold_22 = TextStyle(
      fontSize: setSp(22),
      fontFamily: 'Roboto',
      fontWeight: FontWeight.bold,
      color: ColorUtils.colorBlack);
  static TextStyle textStyleSizeBold_24 = TextStyle(
      fontSize: setSp(24),
      fontFamily: 'Roboto',
      fontWeight: FontWeight.bold,
      color: ColorUtils.colorBlack);
  static TextStyle textStyleSizeBold_26 = TextStyle(
      fontSize: setSp(26),
      fontFamily: 'Roboto',
      fontWeight: FontWeight.bold,
      color: ColorUtils.colorBlack);
  static TextStyle textStyleSizeBold_28 = TextStyle(
      fontSize: setSp(28),
      fontFamily: 'Roboto',
      fontWeight: FontWeight.bold,
      color: ColorUtils.colorBlack);

  static TextStyle textStyle18(BuildContext context) {
    return Theme.of(context).textTheme.button.copyWith(fontSize: setSp(18));
  }
}
