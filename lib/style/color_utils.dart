import 'package:flutter/material.dart';

abstract class ColorUtils {
  static final Color colorRed = Color(0xFFDB4437);
  static final Color colorBlack = Color(0xFF393939);
  static final Color colorGreen = Color(0xFF00833E);
  static final Color colorGreyText = Color(0xFFB2B2B2);
  static final Color colorWhite = Colors.white;
}
