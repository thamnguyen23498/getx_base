import 'package:aibooks/commons/base_page/base_horizontal_tablet.dart';
import 'package:aibooks/commons/screen_config/screen.dart';
import 'package:aibooks/style/color_utils.dart';
import 'package:aibooks/style/font_utils.dart';
import 'package:aibooks/ui/screens/account/signin/login_screen.dart';
import 'package:aibooks/ui/screens/account/signin/signin_viewmodel.dart';
import 'package:aibooks/ui/screens/account/signup/signup_screen.dart';
import 'package:aibooks/ui/widget/custom_button_account.dart';
import 'package:aibooks/ui/widget/custom_scroll_view.dart';
import 'package:aibooks/ui/widget/custom_textfield.dart';
import 'package:aibooks/ui/widget/header_accout.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class SignInPage extends BaseTabletStateful<SignInViewModel> {
  @override
  Widget buildBodyPhone(BuildContext pageContext) {
    scale();
    return Scaffold(
      backgroundColor: Theme.of(pageContext).scaffoldBackgroundColor,
      body: CommonScrollView(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              HeaderAccount(),
              Container(
                height: 2,
                color: ColorUtils.colorRed,
              ),
              buildContent(pageContext),
              CustomBtnAccount(
                  nameBtn: 'Change Theme',
                  onPress: () {
                    viewModel.handleChangeThem();
                  })
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget buildBodyTablet(BuildContext pageContext) {
    scale();
    // TODO: implement buildBodyTablet
    return Scaffold(
      body: Center(
        child: Text(
          'Login',
          style: FontUtils.textStyleSizeBold_20,
        ),
      ),
    );
  }

  Widget buildContent(BuildContext context) {
    return Form(
      key: viewModel.formKey,
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: setWidth(45),
        ),
        width: Screen.width(context),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: setHeight(25)),
            Text(
              'Đăng nhập',
              style: FontUtils.textStyleSizeBold_22.copyWith(color: ColorUtils.colorGreen),
            ),
            SizedBox(height: setHeight(25)),
            customTextField(viewModel.emailController, 'Nhập email', false,
                validator: viewModel.validateEmail()),
            SizedBox(height: setHeight(20)),
            Obx(
              () {
                return customTextField(viewModel.passwordController, 'Nhập mật khẩu', true,
                    validator: viewModel.validatePass());
              },
            ),
            SizedBox(height: setHeight(20)),
            SizedBox(height: setHeight(5)),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GetBuilder<SignInViewModel>(
                  tag: instanceTag,
                  builder: (_) {
                    return Checkbox(
                      checkColor: ColorUtils.colorWhite,
                      activeColor: ColorUtils.colorGreen,
                      value: viewModel.checkbox,
                      onChanged: (value) {
                        viewModel.setCheckBox();
                      },
                    );
                  },
                ),
                GetBuilder<SignInViewModel>(
                    tag: instanceTag,
                    builder: (_) {
                      return Text(
                        'Lưu email',
                        style: FontUtils.textStyleSizeBold_14.copyWith(
                            color: viewModel.checkbox == false
                                ? ColorUtils.colorGreyText
                                : ColorUtils.colorBlack),
                      );
                    }),
                Spacer(),
                TextButton(
                  onPressed: () {},
                  child: Text(
                    'Quên mật khẩu?',
                    style: FontUtils.textStyleSize_16.copyWith(color: ColorUtils.colorGreen),
                  ),
                )
              ],
            ),
            CustomBtnAccount(
              onPress: () {
                Get.to(() => LoginScreen(), transition: Transition.downToUp);
              },
              nameBtn: 'Đăng nhập',
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Bạn chưa có tài khoản?',
                  style: FontUtils.textStyleSizeBold_14.copyWith(color: ColorUtils.colorGreen),
                ),
                GestureDetector(
                  onTap: () {
                    Get.to(() => SignUpScreen(), transition: Transition.rightToLeft);
                  },
                  child: Container(
                    height: setHeight(40),
                    alignment: Alignment.centerLeft,
                    color: Colors.transparent,
                    child: Text(
                      ' Đăng ký ngay',
                      style: FontUtils.textStyleSizeBold_16.copyWith(color: ColorUtils.colorGreen),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: setHeight(30)),
          ],
        ),
      ),
    );
  }

  Widget customTextField(TextEditingController controller, String hintText, bool isPass,
      {Function validator}) {
    return CustomTextField(
      borderSideColor: ColorUtils.colorGreyText,
      widthBorderSide: 2,
      controller: controller,
      isPass: isPass ? viewModel.isPass.value : false,
      maxLine: isPass ? 1 : null,
      borderRadius: setRadius(10),
      hintText: hintText,
      isDense: true,
      label: hintText,
      validator: validator,
      prefixIcon: Padding(
        padding: EdgeInsets.symmetric(horizontal: setWidth(10)),
        child: Icon(
          isPass ? Icons.lock : Icons.mail,
          color: ColorUtils.colorGreyText,
          size: setRadius(24),
        ),
      ),
      suffixIcon: isPass
          ? IconButton(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              disabledColor: Colors.transparent,
              icon: Icon(
                viewModel.isPass == false
                    ? Icons.visibility_outlined
                    : Icons.visibility_off_outlined,
                color: ColorUtils.colorGreyText,
                size: setRadius(20),
              ),
              onPressed: () {
                viewModel.setShowPass();
              },
            )
          : null,
    );
  }

  void scale() {
    // print('height: ${setHeight(1)}');
    // print('width: ${setWidth(1)}');
    // print('fontSize: ${setSp(1)}');
  }
}
