import 'package:aibooks/commons/base_page/base_viewmodel.dart';
import 'package:aibooks/commons/base_page/get_x_util.dart';
import 'package:aibooks/controller/theme_language_controller.dart';
import 'package:aibooks/utils/custom_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class SignInViewModel extends BaseViewModel {
  TextEditingController emailController;
  TextEditingController passwordController;
  RxBool isPass = true.obs;
  bool checkbox = false;
  FocusNode focusNode = new FocusNode();
  final formKey = GlobalKey<FormState>();
  bool isLightTheme = true;
  ThemeAndLanguageController themeAndLanguageController = GetXUtil.instance();

  @override
  void onInit() {
    // TODO: implement onInit
    emailController = new TextEditingController();
    passwordController = new TextEditingController();
    super.onInit();
  }

  void setShowPass() {
    isPass.value = !isPass.value;
  }

  void setCheckBox() {
    checkbox = !checkbox;
    update();
  }

  dynamic validateEmail() {
    Function(String) validator = (String value) {
      if (value.trim().isEmpty || value == null) {
        return 'Vui lòng nhập mail';
      } else if (GetUtils.isEmail(value) == false) {
        return 'Sai định dạng email';
      } else {
        return null;
      }
    };
    return validator;
  }

  dynamic validatePass() {
    Function(String) validator = (String value) {
      if (value.trim().isEmpty || value == null) {
        return 'Vui lòng nhập mật khẩu';
      } else {
        return null;
      }
    };
    return validator;
  }

  void handleLogin() {
    if (formKey.currentState.validate()) {
      CustomToast.showToastSuccess('Đăng nhập thành công');
    }
  }

  void handleChangeThem() {
    isLightTheme = !isLightTheme;
    themeAndLanguageController.changeTheme(isLightTheme);
    update();
  }
}
