import 'package:aibooks/commons/base_page/base_page_stateful.dart';
import 'package:aibooks/commons/screen_config/screen.dart';
import 'package:aibooks/ui/widget/bg_header.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'login_viewmodel.dart';

// ignore: must_be_immutable
class LoginScreen extends BasePageStateful<LoginViewModel> {
  @override
  Widget buildBody(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
        ),
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
              BGHeaderWidget(
                child: SizedBox(),
                hasBorderRadius: true,
              )
            ],
          ),
        ));
  }
}
