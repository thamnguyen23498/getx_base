import 'package:aibooks/commons/base_page/base_page_stateless.dart';
import 'package:aibooks/commons/screen_config/screen.dart';
import 'package:aibooks/generated/assets.dart';
import 'package:aibooks/generated/l10n.dart';
import 'package:aibooks/style/font_utils.dart';
import 'package:aibooks/ui/screens/account/changepassword/change_pass_viewmodel.dart';
import 'package:aibooks/utils/custom_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class LoginScreen extends BasePageStateless<ChangePassVM> {
  @override
  Widget buildBody(BuildContext context) {
    // TODO: implement buildBody
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).key),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            GetBuilder<ChangePassVM>(
                tag: instanceTag,
                builder: (_) {
                  return Text(
                    viewModel.test.toString(),
                    style: FontUtils.textStyleSize_18,
                  );
                }),
            SizedBox(
              height: setHeight(30),
            ),
            GestureDetector(
              onTap: () {
                // apiLogin();
                // Get.to(() => LoginScreen());
                viewModel.changeValue();
                CustomToast.showToastSuccess('Đăng nhập thành công');
              },
              child: SvgPicture.asset(
                Assets.iconsIconMicrophone,
                fit: BoxFit.cover,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
