import 'package:aibooks/commons/base_page/base_viewmodel.dart';

class ChangePassVM extends BaseViewModel {
  bool test = false;

  void changeValue() {
    test = !test;
    update();
  }
}
