import 'package:aibooks/commons/base_page/base_viewmodel.dart';
import 'package:aibooks/utils/custom_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get_utils/src/get_utils/get_utils.dart';

class SignUpViewModel extends BaseViewModel {
  TextEditingController emailController;
  TextEditingController passwordController;
  bool isPass = true;
  final formKey = GlobalKey<FormState>();

  @override
  void onInit() {
    emailController = new TextEditingController();
    passwordController = new TextEditingController();
    super.onInit();
  }

  void setShowPass() {
    isPass = !isPass;
    update();
  }

  dynamic validateEmail() {
    Function(String) validator = (String value) {
      if (value.trim().isEmpty || value == null) {
        return 'Vui lòng nhập mail';
      } else if (GetUtils.isEmail(value) == false) {
        return 'Sai định dạng email';
      } else {
        return null;
      }
    };
    return validator;
  }

  dynamic validatePass() {
    Function(String) validator = (String value) {
      if (value.trim().isEmpty || value == null) {
        return 'Vui lòng nhập mật khẩu';
      } else {
        return null;
      }
    };
    return validator;
  }

  void handleSignUp() {
    if (formKey.currentState.validate()) {
      CustomToast.showToastSuccess('Đăng nhập thành công');
    }
  }
}
