import 'package:aibooks/commons/base_page/base_page_stateless.dart';
import 'package:aibooks/commons/screen_config/screen.dart';
import 'package:aibooks/style/color_utils.dart';
import 'package:aibooks/style/font_utils.dart';
import 'package:aibooks/ui/screens/account/signup/signup_viewmodel.dart';
import 'package:aibooks/ui/widget/custom_button_account.dart';
import 'package:aibooks/ui/widget/custom_scroll_view.dart';
import 'package:aibooks/ui/widget/custom_textfield.dart';
import 'package:aibooks/ui/widget/header_accout.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class SignUpScreen extends BasePageStateless<SignUpViewModel> {
  @override
  Widget buildBody(BuildContext context) {
    // TODO: implement buildBody
    return Scaffold(
      body: CommonScrollView(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              HeaderAccount(),
              Container(
                height: 2,
                color: ColorUtils.colorRed,
              ),
              buildContent(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildContent(BuildContext context) {
    return Form(
      key: viewModel.formKey,
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: setWidth(45),
        ),
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: setHeight(35)),
            Text(
              'Đăng kí',
              style: FontUtils.textStyleSizeBold_22.copyWith(color: ColorUtils.colorGreen),
            ),
            SizedBox(height: setHeight(35)),
            customTextField(viewModel.emailController, 'Nhập email', false,
                validator: viewModel.validateEmail()),
            SizedBox(height: setHeight(25)),
            GetBuilder<SignUpViewModel>(
              tag: instanceTag,
              builder: (_) {
                return customTextField(viewModel.passwordController, 'Nhập mật khẩu', true,
                    validator: viewModel.validatePass());
              },
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                TextButton(
                  onPressed: () {
                    Get.back();
                  },
                  child: Text(
                    'Quay lại',
                    style: FontUtils.textStyleSize_16.copyWith(color: ColorUtils.colorGreen),
                  ),
                )
              ],
            ),
            CustomBtnAccount(
              margin: EdgeInsets.symmetric(vertical: setHeight(25)),
              onPress: () {
                viewModel.handleSignUp();
              },
              nameBtn: 'Đăng kí',
            ),
          ],
        ),
      ),
    );
  }

  Widget customTextField(TextEditingController controller, String hintText, bool isPass,
      {Function validator}) {
    return CustomTextField(
      borderSideColor: ColorUtils.colorGreyText,
      widthBorderSide: 2,
      controller: controller,
      isPass: isPass ? viewModel.isPass : false,
      maxLine: isPass ? 1 : null,
      borderRadius: 12,
      hintText: hintText,
      label: hintText,
      isDense: true,
      validator: validator,
      prefixIcon: Padding(
        padding: EdgeInsets.symmetric(horizontal: setWidth(10)),
        child: Icon(
          isPass ? Icons.lock : Icons.mail,
          color: ColorUtils.colorGreyText,
          size: setRadius(24),
        ),
      ),
      suffixIcon: isPass
          ? IconButton(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              disabledColor: Colors.transparent,
              icon: Icon(
                viewModel.isPass == false
                    ? Icons.visibility_outlined
                    : Icons.visibility_off_outlined,
                color: ColorUtils.colorGreyText,
                size: setRadius(20),
              ),
              onPressed: () {
                viewModel.setShowPass();
              },
            )
          : SizedBox(),
    );
  }
}
