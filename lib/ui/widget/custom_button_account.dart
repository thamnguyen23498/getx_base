import 'package:aibooks/commons/screen_config/screen.dart';
import 'package:aibooks/style/color_utils.dart';
import 'package:aibooks/style/font_utils.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CustomBtnAccount extends StatelessWidget {
  CustomBtnAccount(
      {this.heightBtn,
      this.radius,
      @required this.nameBtn,
      @required this.onPress,
      this.colorBtn,
      this.widthBtn,
      this.colorText,
      this.margin,
      this.textStyle});

  double heightBtn;
  double radius;
  String nameBtn;
  VoidCallback onPress;
  Color colorBtn;
  double widthBtn;
  Color colorText;
  EdgeInsets margin;
  TextStyle textStyle;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widthBtn ?? double.infinity,
      height: heightBtn ?? 50,
      margin: margin ?? EdgeInsets.only(top: setHeight(25), bottom: setHeight(25)),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: colorBtn ?? ColorUtils.colorGreen,
          onPrimary: Colors.white,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(radius ?? 10.0)),
        ),
        onPressed: onPress,
        child: Text(nameBtn, style: FontUtils.textStyle18(context)),
      ),
    );
  }
}
