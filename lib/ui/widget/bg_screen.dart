import 'package:aibooks/commons/screen_config/screen.dart';
import 'package:aibooks/generated/assets.dart';
import 'package:aibooks/style/color_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BGScreenWidget extends StatelessWidget {
  String imgBg;
  Widget child;

  BGScreenWidget({this.imgBg, @required this.child});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image.asset(
          imgBg ?? Assets.imagesBgLogin,
          width: Screen.width(context),
          height: Screen.height(context),
          fit: BoxFit.cover,
        ),
        Container(
          width: Screen.width(context),
          height: Screen.height(context),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: FractionalOffset.topCenter,
              end: FractionalOffset.bottomCenter,
              colors: [
                Colors.transparent,
                ColorUtils.colorBlack.withOpacity(0.8),
                // Colors.black,
              ],
              stops: [0.2, 1.0],
            ),
          ),
          child: child,
        )
      ],
    );
  }
}
