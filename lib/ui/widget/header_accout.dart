import 'package:aibooks/commons/screen_config/screen.dart';
import 'package:aibooks/generated/assets.dart';
import 'package:aibooks/style/color_utils.dart';
import 'package:aibooks/style/font_utils.dart';
import 'package:flutter/material.dart';

class HeaderAccount extends StatelessWidget {
  const HeaderAccount({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          stops: [0.2, 0.8],
          colors: [
            Color(0xFF182924),
            Color(0xFF135945),
          ],
        ),
      ),
      padding: EdgeInsets.symmetric(horizontal: setWidth(15), vertical: setHeight(15)),
      child: Column(
        children: [
          SizedBox(height: MediaQuery.of(context).size.height * 0.13),
          Image.asset(
            Assets.imagesLogoPng,
            height: setHeight(80),
            fit: BoxFit.fitHeight,
          ),
          SizedBox(height: MediaQuery.of(context).size.height * 0.05),
          Text(
            'Chào buổi sáng',
            style: FontUtils.textStyleSizeBold_22.copyWith(color: ColorUtils.colorWhite),
          ),
          SizedBox(height: setHeight(25)),
        ],
      ),
    );
  }
}
