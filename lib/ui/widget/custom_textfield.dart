import 'package:aibooks/commons/base_page/base_page_stateless.dart';
import 'package:aibooks/commons/instance_tag.dart';
import 'package:aibooks/commons/screen_config/screen.dart';
import 'package:aibooks/style/color_utils.dart';
import 'package:aibooks/style/font_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class CustomTextField extends StatelessWidget {
  bool isPass = false;
  TextStyle style;
  num width;
  num height;
  TextInputType inputType;
  String hintText;
  Color fillColor;
  TextStyle hintStyle;
  Widget prefixIcon;
  EdgeInsets contentPadding;
  Widget suffixIcon;
  Color borderSideColor;
  Function validator;
  TextEditingController controller;
  int maxLength;
  List<TextInputFormatter> textInputFormatter;
  double borderRadius;
  bool enable = true;
  bool isDense = false;
  double errBorderRadius;
  int maxLine;
  int minLine;
  String label;
  TextInputAction textInputAction;
  Widget count;
  Function onChange;
  double widthBorderSide;
  RxBool visibilityLabel = false.obs;

  CustomTextField(
      {this.isPass,
      this.style,
      this.inputType,
      this.hintText,
      this.fillColor,
      this.hintStyle,
      this.prefixIcon,
      this.contentPadding,
      this.suffixIcon,
      this.borderSideColor,
      this.validator,
      this.controller,
      this.maxLength,
      this.textInputFormatter,
      this.borderRadius,
      this.width,
      this.height,
      this.enable,
      this.isDense,
      this.errBorderRadius,
      this.maxLine,
      this.minLine,
      this.label,
      this.textInputAction,
      this.count,
      this.onChange,
      this.widthBorderSide});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      enableInteractiveSelection: false,
      enabled: enable == null ? true : enable,
      controller: controller,
      style: style == null ? FontUtils.textStyleSize_18 : style,
      keyboardType: inputType,
      maxLength: maxLength,
      validator: validator,
      obscureText: isPass == null ? false : isPass,
      cursorColor: Colors.black,
      maxLines: maxLine,
      minLines: minLine,
      textInputAction: textInputAction == null ? TextInputAction.done : textInputAction,
      inputFormatters: textInputFormatter,
      keyboardAppearance: Brightness.light,
      onChanged: onChange,
      decoration: InputDecoration(
        isDense: isDense == null ? false : isDense,
        fillColor: fillColor == null ? ColorUtils.colorWhite : fillColor,
        alignLabelWithHint: false,
        labelStyle:
            FontUtils.textStyleSize_16.copyWith(color: ColorUtils.colorBlack.withOpacity(0.7)),
        filled: true,
        labelText: label,
        hintText: hintText == null ? '' : hintText,
        hintStyle: hintStyle == null
            ? FontUtils.textStyleSize_16.copyWith(color: ColorUtils.colorGreyText)
            : hintStyle,
        focusColor: Colors.black,
        prefixIcon: prefixIcon == null ? null : prefixIcon,
        contentPadding: contentPadding == null
            ? EdgeInsets.symmetric(vertical: setHeight(15), horizontal: setWidth(5))
            : contentPadding,
        suffixIcon: suffixIcon == null ? null : suffixIcon,
        errorMaxLines: 3,
        focusedErrorBorder: buildSetInputBorder(borderRadius: borderRadius, color: borderSideColor),
        counter: count,
        disabledBorder: buildSetInputBorder(
          color: borderSideColor,
          borderRadius: borderRadius,
        ),
        focusedBorder: buildSetInputBorder(
          color: borderSideColor,
          borderRadius: borderRadius,
        ),
        errorBorder: buildSetInputBorder(
          color: ColorUtils.colorRed,
          borderRadius: borderRadius,
        ),
        enabledBorder: buildSetInputBorder(
          color: borderSideColor,
          borderRadius: borderRadius,
        ),
        errorStyle: FontUtils.textStyleSize_14.copyWith(
          color: ColorUtils.colorRed,
        ),
        border: buildSetInputBorder(
          color: borderSideColor,
          borderRadius: borderRadius,
        ),
      ),
    );
  }

  buildSetInputBorder({Color color, double borderRadius}) {
    return OutlineInputBorder(
      borderSide: BorderSide(
          width: widthBorderSide ?? 1,
          color: color == null ? ColorUtils.colorWhite : color,
          style: BorderStyle.solid),
      borderRadius: BorderRadius.all(
        Radius.circular(borderRadius == null ? 0 : borderRadius),
      ),
    );
  }
}
