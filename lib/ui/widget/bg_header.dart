import 'package:aibooks/commons/screen_config/screen.dart';
import 'package:aibooks/generated/assets.dart';
import 'package:aibooks/style/color_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BGHeaderWidget extends StatelessWidget {
  String imgBg;
  double height;
  Widget child;
  bool hasBorderRadius;

  BGHeaderWidget({
    this.imgBg,
    this.height,
    @required this.child,
    @required this.hasBorderRadius,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.only(bottomRight: Radius.circular(hasBorderRadius ? 100 : 0)),
          child: Image.asset(
            imgBg ?? Assets.imagesBgLogin,
            width: Screen.width(context),
            height: height ?? Screen.height(context) * 0.35,
            fit: BoxFit.fitWidth,
          ),
        ),
        Container(
          width: Screen.width(context),
          height: height ?? Screen.height(context) * 0.35,
          padding: EdgeInsets.symmetric(horizontal: setWidth(15), vertical: setHeight(15)),
          decoration: BoxDecoration(
            borderRadius:
                BorderRadius.only(bottomRight: Radius.circular(hasBorderRadius ? 100 : 0)),
            gradient: LinearGradient(
              begin: FractionalOffset.topCenter,
              end: FractionalOffset.bottomCenter,
              colors: [
                Colors.transparent,
                ColorUtils.colorBlack.withOpacity(0.8),
                // Colors.black,
              ],
              stops: [0.2, 1.0],
            ),
          ),
          child: child,
        )
      ],
    );
  }
}
