import 'package:aibooks/commons/screen_config/screen.dart';
import 'package:aibooks/style/color_utils.dart';
import 'package:aibooks/style/font_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class CustomToast {
  static void showToastSuccess(String message, {Color colorText}) {
    Get.snackbar(
      '',
      '',
      backgroundColor: ColorUtils.colorGreen.withOpacity(0.7),
      borderRadius: setWidth(0),
      padding: EdgeInsets.only(top: 5, bottom: 10),
      margin: EdgeInsets.zero,
      titleText: SizedBox(),
      duration: Duration(seconds: 1, milliseconds: 500),
      snackPosition: SnackPosition.BOTTOM,
      messageText: Container(
        constraints: BoxConstraints(
          minHeight: setHeight(25),
        ),
        alignment: Alignment.center,
        child: Text(
          message,
          textAlign: TextAlign.center,
          style: FontUtils.textStyleSize_14.copyWith(
            color: colorText ?? ColorUtils.colorWhite,
          ),
        ),
      ),
    );
  }

  static void showToastFail(String message, {Color colorText}) {
    Get.snackbar(
      '',
      '',
      backgroundColor: ColorUtils.colorRed,
      borderRadius: setWidth(0),
      padding: EdgeInsets.only(top: 5, bottom: 10),
      margin: EdgeInsets.zero,
      titleText: SizedBox(),
      duration: Duration(seconds: 1, milliseconds: 500),
      snackPosition: SnackPosition.BOTTOM,
      messageText: Container(
        constraints: BoxConstraints(
          minHeight: setHeight(25),
        ),
        alignment: Alignment.center,
        child: Text(
          message,
          textAlign: TextAlign.center,
          style: FontUtils.textStyleSize_14.copyWith(
            color: colorText ?? ColorUtils.colorWhite,
          ),
        ),
      ),
    );
  }
}
